<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_recommandations_attribut' => 'Ajouter cet attribut de recommandation',

	// C
	'champ_identifiant_label' => 'Identifiant',
	'champ_liens_label' => 'Contenus liés',
	'champ_ponderation_label' => 'Pondération',
	'champ_titre_label' => 'Titre',
	'champ_type_label' => 'Type',
	'confirmer_supprimer_recommandations_attribut' => 'Confirmez-vous la suppression de cet attribut de recommandation ?',

	// I
	'icone_creer_recommandations_attribut' => 'Créer un attribut de recommandation',
	'icone_modifier_recommandations_attribut' => 'Modifier ce attribut de recommandation',
	'info_1_recommandations_attribut' => 'Un attribut de recommandation',
	'info_aucun_recommandations_attribut' => 'Aucun attribut de recommandation',
	'info_nb_recommandations_attributs' => '@nb@ attributs de recommandation',
	'info_recommandations_attributs_auteur' => 'Les attributs de recommandation de cet auteur',

	// R
	'retirer_lien_recommandations_attribut' => 'Retirer cet attribut de recommandation',
	'retirer_tous_liens_recommandations_attributs' => 'Retirer tous les attributs de recommandation',

	// S
	'supprimer_recommandations_attribut' => 'Supprimer cet attribut de recommandation',

	// T
	'texte_ajouter_recommandations_attribut' => 'Ajouter un attribut de recommandation',
	'texte_changer_statut_recommandations_attribut' => 'Cet attribut de recommandation est :',
	'texte_creer_associer_recommandations_attribut' => 'Créer et associer un attribut de recommandation',
	'texte_definir_comme_traduction_recommandations_attribut' => 'Ce attribut de recommandation est une traduction du attribut de recommandation numéro :',
	'titre_langue_recommandations_attribut' => 'Langue de ce attribut de recommandation',
	'titre_logo_recommandations_attribut' => 'Logo de cet attribut de recommandation',
	'titre_objets_lies_recommandations_attribut' => 'Liés à cet attribut de recommandation',
	'titre_page_recommandations_attributs' => 'Les attributs de recommandation',
	'titre_recommandations_attribut' => 'Attribut de recommandation',
	'titre_recommandations_attributs' => 'Attributs de recommandation',
	'titre_recommandations_attributs_rubrique' => 'Attributs de recommandation de la rubrique',
];
