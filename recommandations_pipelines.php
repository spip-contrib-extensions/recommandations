<?php

/**
 * Utilisations de pipelines par Recommandations
 *
 * @plugin     Recommandations
 * @copyright  2024
 * @author     RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Recommandations\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function recommandations_affiche_milieu($flux) {
	$texte = '';
	$e = trouver_objet_exec($flux['args']['exec']);

	// recommandations_attributs TOUS les objets
	if (
		$e
		and !$e['edition']
		//and in_array($e['type'], ['article'])
	) {
		$texte .= recuperer_fond('prive/objets/editer/liens', [
			'table_source' => 'recommandations_attributs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		]);
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Supprime les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function recommandations_optimiser_base_disparus($flux) {

	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(['recommandations_attribut' => '*'], '*');

	return $flux;
}

/**
 * Essayer de trouver un type aux attributs quand on peut
 */
function recommandations_pre_edition($flux) {
	if (
		$flux['args']['action'] == 'modifier'
		and $flux['args']['type'] == 'recommandations_attribut'
		// Si on demande à changer l'identifiant
		and $flux['data']['identifiant']
		// seulement si on a pas déjà typé explicitement en même temps que l'identifiant
		and empty($flux['data']['type'])
		// et que l'identifiant est une URL
		and filter_var($flux['data']['identifiant'], FILTER_VALIDATE_URL)
	) {
		// Extraire un type de l'identifiant
		$domaine = parse_url($flux['data']['identifiant'], PHP_URL_HOST);
		$domaine = explode('.', $domaine);
		$extension = array_pop($domaine);
		$type = array_pop($domaine); 
		$flux['data']['type'] = $type;
	}
	
	return $flux;
}

/**
 * Insertion dans le pipeline boite_infos (SPIP)
 *
 * Ajouter un lien pour réindexer les attributs
 *
 * @param array $flux
 * 		Le contexte du pipeline
 * @return array $flux
 * 		Le contexte modifié
 */
function recommandations_boite_infos($flux) {
	if ($objet = $flux['args']['type'] and $id_objet = intval($flux['args']['id'])) {
			$flux['data'] .= filtrer(
				'bouton_action_horizontal',
				generer_action_auteur('recommandations_indexer_objet', "$objet-$id_objet", self()),
				_T('recommandations:reindexer'),
				'recommandations_attribut-24.svg',
				'',
				'btn_link'
			);
	}
	
	return $flux;
}
