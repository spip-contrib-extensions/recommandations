<?php

namespace Spip\Cli\Command;

use Spip\Cli\Console\Command;
use Spip\Cli\Console\Style\SpipCliStyle;
use Spip\Cli\Loader\Spip;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\ProgressBar;

class RecommandationsIndexer extends Command
{
	protected function configure() {
		$this
			->setName('recommandations:indexer')
			->setDescription('Scanner les contenus SPIP pour peupler les attributs de recommandation')
			->addOption(
				'exclure_objets',
				'x',
				InputOption::VALUE_REQUIRED,
				'Ne pas scanner ces objets'
			)
			->addOption(
				'inclure_objets',
				'i',
				InputOption::VALUE_REQUIRED,
				'Ne scanner que ces objets'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io->title($this->getDescription());
		$this->demarrerSpip();
		
		$tables_objets = lister_tables_objets_sql();
		$recommandations_indexer = charger_fonction('indexer', 'inc/recommandations');
		
		if ($exclure_objets = $input->getOption('exclure_objets')) {
			// On s'assure d'avoir des tables SQL
			$exclure_objets = explode(',', $exclure_objets);
			$exclure_objets = array_map('trim', $exclure_objets);
			$exclure_objets = array_map('table_objet_sql', $exclure_objets);
			$exclure_objets = array_filter($exclure_objets);
			$exclure_objets = array_flip($exclure_objets);
			
			// On retire de la liste
			$tables_objets = array_diff_key($tables_objets, $exclure_objets);
		}
		
		if ($inclure_objets = $input->getOption('inclure_objets')) {
			// On s'assure d'avoir des tables SQL
			$inclure_objets = explode(',', $inclure_objets);
			$inclure_objets = array_map('trim', $inclure_objets);
			$inclure_objets = array_map('table_objet_sql', $inclure_objets);
			$inclure_objets = array_filter($inclure_objets);
			$inclure_objets = array_flip($inclure_objets);
			
			// On retire de la liste
			$tables_objets = array_intersect_key($tables_objets, $inclure_objets);
		}
		
		// On parcourt chaque objet SPIP restant
		foreach ($tables_objets as $table => $desc) {
			$cle_objet = id_table_objet($table);
			$objet = objet_type($table);
			
			if ($tout = sql_allfetsel($cle_objet, $table)) {
				$nb = count($tout);
				$output->writeln("<info>$objet : $nb à indexer</info>");
				// On génère une barre de progresion
				$progressBar = new ProgressBar($output, $nb);
				$progressBar->setFormat('very_verbose');
				$progressBar->start();
				
				foreach ($tout as $ligne) {
					$ok = $recommandations_indexer($objet, $ligne[$cle_objet]);
					// On avance d'un cran
					$progressBar->advance();
				}
				
				$progressBar->finish();
				$output->writeln("\n\n");
			}
		}
		
		return Command::SUCCESS;
	}
}
