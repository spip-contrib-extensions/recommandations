# Recommandations

Ce plugin fournit différents outils, dont deux API, pour aider à gérer de manière générique des recommandations entre contenus : 
- une API "recommandations" qui doit renvoyer une liste de contenus intéressants en rapport avec un contenu source
- une table "recommandations_attributs" pour lister des attributs arbitraires et sa table de liens pour lier aux contenus
- une API "recommandations/indexer" qui sert, quand on en a besoin, à indexer un contenu précis (objet, id_objet) en lui assignant des attributs pondérés

## API recommandations

Cette API se propose d'avoir un appel générique unique pour récupérer une liste de contenus intéressants en rapport avec un autre. Mais en arrière-plan, la manière de définir quels doivent être ces contenus est laissé libre à l'implémentation de chacun.

### Utilisation

``` php
$recommandations = charger_fonction('recommandations', 'inc');
$recommandations('patate', 1234, $options);
```

Les options prévues pour le moment :
- inclure_objets : liste explicites des objets à recommander
- exclure_objets : liste d'objets à ne pas recommander
- exclure_contenus : liste de contenus précis (couple objet+id_objet) à ne pas recommander
- limite : nombre maximum de recommandations à renvoyer

### Implémentation

Il est possible d'implémenter une méthode propre, un algorithme dédié, pour chaque cas suivant : 
- s'il n'y a qu'un unique objet à recommander
  - `recommandations_pour_<objetsource>_<objetàrecommander>[_dist]($id_objet, $option)`
  - `recommandations_<objetàrecommander>[_dist]($objet, $id_objet, $options)`
- `recommandations_pour_<objetsource>[_dist]($id_objet, $options)`
- `recommandations_defaut[_dist]($objet, $id_objet, $options)`

## API recommandations/indexer

Cette API permet de définir quoi faire quand on veut réindexer un contenu précis (objet-id_objet).

Il est possible d'implémenter deux fonctions :
- `recommandations_<objet>_indexer[_dist]($id_objet)`
- `recommandations_defaut[_dist]($objet, $id_objet)`