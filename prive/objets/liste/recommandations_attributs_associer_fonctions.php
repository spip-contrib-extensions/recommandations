<?php

/**
 * Fonctions du squelette associé
 *
 * @plugin     Recommandations
 * @copyright  2024
 * @author     RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Recommandations\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');
