<?php

/**
 * Fonctions utiles au plugin Recommandations
 *
 * @plugin     Recommandations
 * @copyright  2024
 * @author     RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Recommandations\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Filtre pour utiliser l'API depuis un squelette
 */
function filtre_recommandations_dist(int $id_objet, string $objet, array $options = []) {
	$recommandations = charger_fonction('recommandations', 'inc');
	return $recommandations($objet, $id_objet, $options);
}
