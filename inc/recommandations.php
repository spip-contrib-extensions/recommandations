<?php

/**
 * Recommande des contenus SPIP par rapport à un contenu source
 * 
 * @param string objet
 * 		Type de l'objet source
 * @param int $id_objet
 * 		ID de l'objet source
 * @param array $options
 * 		Tableau d'options
 * 		- array inclure_objets
 * 			Liste explicites des objets à recommander
 * 		- array exclure_objets
 * 			Liste d'objets à ne pas recommander
 * 		- array exclure_contenus
 * 			Liste de contenus précis (couple objet+id_objet) à ne pas recommander
 * 		- int limite
 * 			Nombre maximum de recommandations à renvoyer
 * @return array
 * 		Retourne un tableau de tableaux contenant les objet-id_objet des contenus recommandés
 */
function inc_recommandations_dist(string $objet, int $id_objet, array $options = []) {
	$recommandations = [];
	$fonction = '';
	
	// Est-ce qu'on demande les recommandations d'un unique objet précis ?
	if (
		isset($options['inclure_objets'])
		and count($options['inclure_objets']) == 1
		and $recommander_objet = reset($options['inclure_objets'])
		and $objet
		and $fonction = charger_fonction($recommander_objet, 'recommandations/pour_' . $objet, true)
	) {
		$recommandations = $fonction($id_objet, $options);
	}
	// Sinon que pour l'objet à recommander
	elseif ($recommander_objet ?? '' and $fonction = charger_fonction($recommander_objet, 'recommandations', true)) {
		$recommandations = $fonction($objet, $id_objet, $options);
	}
	// Sinon que pour l'objet source
	elseif ($objet and $fonction = charger_fonction('pour_' . $objet, 'recommandations', true)) {
		$recommandations = $fonction($id_objet, $options);
	}
	// Sinon défaut générique
	elseif ($fonction = charger_fonction('defaut', 'recommandations', true)) {
		$recommandations = $fonction($objet, $id_objet, $options);
	}
	
	// On passe le tout dans un pipeline
	$recommandations = pipeline(
		'recommandations',
		[
			'args' => [
				'id_objet' => $id_objet,
				'objet' => $objet,
				'options' => $options,
			],
			'data' => $recommandations
		]
	);
	
	// Doit-on limiter le résultat ?
	$limite = intval($options['limite'] ?? 0);
	if ($limite > 0) {
		$recommandations = array_slice($recommandations, 0, $limite);
	}
	
	return $recommandations;
}

/**
 * Enregistre des informations utiles pour recommander un contenu
 * 
 * @param string objet
 * 		Type de l'objet source
 * @param int $id_objet
 * 		ID de l'objet source
 */
function inc_recommandations_indexer_dist(string $objet, int $id_objet) {
	$fonction = '';
	
	// Pas de mélange des genres, on commence par nettoyer tous les attributs de cet objet là
	recommandations_objet_nettoyer_attributs($objet, $id_objet);
	
	if ($fonction = charger_fonction('indexer', 'recommandations/' . $objet, true)) {
		return $fonction($id_objet);
	}
	elseif ($fonction = charger_fonction('indexer', 'recommandations/defaut', true)) {
		return $fonction($objet, $id_objet);
	}
	
	return false;
}

function recommandations_objet_nettoyer_attributs(string $objet, int $id_objet) {
	include_spip('action/editer_liens');
	objet_dissocier(
		['recommandations_attribut' => '*'],
		[$objet => $id_objet]
	);
}

function recommandations_objet_ajouter_attribut(string $objet, int $id_objet, array $attribut) {
	include_spip('action/editer_objet');
	
	if ($identifiant = $attribut['identifiant'] ?? '') {
		// On cherche s'il existe déjà
		if (!$id_recommandations_attribut = sql_getfetsel('id_recommandations_attribut', 'spip_recommandations_attributs', 'identifiant = '.sql_quote($identifiant))) {
			// Sinon on le créé
			$id_recommandations_attribut = objet_inserer('recommandations_attribut');
			$erreur = objet_modifier(
				'recommandations_attribut',
				$id_recommandations_attribut,
				[
					'identifiant' => $attribut['identifiant'],
					'titre' => $attribut['titre'] ?? $attribut['identifiant'],
				]
			);
		}
		
		// Si on a bien un attribut dans la base, on fait le lien
		if ($id_recommandations_attribut) {
			include_spip('action/editer_liens');
			objet_associer(
				['recommandations_attribut' => $id_recommandations_attribut],
				[$objet => $id_objet],
				['ponderation' => intval($attribut['ponderation'] ?? 1)]
			);
		}
	}
	
	return false;
}
