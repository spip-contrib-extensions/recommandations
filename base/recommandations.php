<?php

/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Recommandations
 * @copyright  2024
 * @author     RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Recommandations\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function recommandations_declarer_tables_interfaces($interfaces) {
	$interfaces['table_des_tables']['recommandations_attributs'] = 'recommandations_attributs';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function recommandations_declarer_tables_objets_sql($tables) {
	$tables['spip_recommandations_attributs'] = [
		'type' => 'recommandations_attribut',
		'principale' => 'oui',
		'page' => false,
		'table_objet_surnoms' => ['recommandationsattribut'], // table_objet('recommandations_attribut') => 'recommandations_attributs' 
		'field' => [
			'id_recommandations_attribut' => 'bigint(21) NOT NULL',
			'identifiant'        => 'varchar(255) NOT NULL DEFAULT ""', // Identifiant sémantique unique de la métadonnée
			'type'               => 'varchar(255) NOT NULL DEFAULT ""', // Type de la métadonnée, son référentiel ou autre
			'titre'              => 'text NOT NULL DEFAULT ""',
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_recommandations_attribut',
		],
		'titre' => 'titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => ['identifiant', 'type', 'titre'],
		'champs_versionnes' => ['identifiant', 'type', 'titre'],
		'rechercher_champs' => ["identifiant" => 10, "type" => 5, "titre" => 10],
		'tables_jointures'  => ['spip_recommandations_attributs_liens'],
	];

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function recommandations_declarer_tables_auxiliaires($tables) {
	$tables['spip_recommandations_attributs_liens'] = [
		'field' => [
			'id_recommandations_attribut' => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'varchar(25) DEFAULT "" NOT NULL',
			'ponderation'        => 'int not null default 1', // À quel point cet attribut est important pour cet objet
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_recommandations_attribut,id_objet,objet',
			'KEY id_recommandations_attribut' => 'id_recommandations_attribut',
		]
	];

	return $tables;
}
