<?php

/**
 * Gestion du formulaire de d'édition de recommandations_attribut
 *
 * @plugin     Recommandations
 * @copyright  2024
 * @author     RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Recommandations\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de recommandations_attribut
 *
 * @param int|string $id_recommandations_attribut
 *     Identifiant du recommandations_attribut. 'new' pour un nouveau recommandations_attribut.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier le recommandations_attribut créé à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'un recommandations_attribut source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du recommandations_attribut, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array[]
 *     Saisies du formulaire
 */
function formulaires_editer_recommandations_attribut_saisies_dist($id_recommandations_attribut = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$saisies = [
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'titre',
				
				'label' => _T('recommandations_attribut:champ_titre_label'),
				
				
			],
		],

		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'identifiant',
				 'obligatoire' => 'oui',
				'label' => _T('recommandations_attribut:champ_identifiant_label'),
				
				
			],
		],

		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'type',
				 'obligatoire' => 'oui',
				'label' => _T('recommandations_attribut:champ_type_label'),
				
				
			],
		],

	];
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_recommandations_attribut
 *     Identifiant du recommandations_attribut. 'new' pour un nouveau recommandations_attribut.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier le recommandations_attribut créé à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'un recommandations_attribut source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du recommandations_attribut, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_recommandations_attribut_identifier_dist($id_recommandations_attribut = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return json_encode([intval($id_recommandations_attribut), $associer_objet]);
}

/**
 * Chargement du formulaire d'édition de recommandations_attribut
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_recommandations_attribut
 *     Identifiant du recommandations_attribut. 'new' pour un nouveau recommandations_attribut.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier le recommandations_attribut créé à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'un recommandations_attribut source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du recommandations_attribut, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_recommandations_attribut_charger_dist($id_recommandations_attribut = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('recommandations_attribut', $id_recommandations_attribut, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de recommandations_attribut
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_recommandations_attribut
 *     Identifiant du recommandations_attribut. 'new' pour un nouveau recommandations_attribut.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier le recommandations_attribut créé à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'un recommandations_attribut source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du recommandations_attribut, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_recommandations_attribut_verifier_dist($id_recommandations_attribut = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('recommandations_attribut', $id_recommandations_attribut, ['identifiant', 'type']);


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de recommandations_attribut
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_recommandations_attribut
 *     Identifiant du recommandations_attribut. 'new' pour un nouveau recommandations_attribut.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier le recommandations_attribut créé à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'un recommandations_attribut source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du recommandations_attribut, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_recommandations_attribut_traiter_dist($id_recommandations_attribut = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$retours = formulaires_editer_objet_traiter('recommandations_attribut', $id_recommandations_attribut, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_recommandations_attribut = $retours['id_recommandations_attribut']) {
		list($objet, $id_objet) = explode('|', $associer_objet);

		if ($objet and $id_objet and autoriser('modifier', $objet, $id_objet)) {
			include_spip('action/editer_liens');

			objet_associer(['recommandations_attribut' => $id_recommandations_attribut], [$objet => $id_objet]);

			if (isset($retours['redirect'])) {
				$retours['redirect'] = parametre_url($retours['redirect'], 'id_lien_ajoute', $id_recommandations_attribut, '&');
			}
		}
	}

	return $retours;
}
