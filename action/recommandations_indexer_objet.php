<?php

/**
 * Indexer les attributs de recommandation d'un contenu
 *
 * @plugin     Recommandations
 * @copyright  2024
 * @author     RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Recommandations\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour indexer les attributs de recommandation d'un contenu
 *
 * @param null|int $arg
 *     objet-id_objet.
 */
function action_recommandations_indexer_objet_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	
	list($objet, $id_objet) = explode('-', $arg);
	
	if ($objet and $id_objet = intval($id_objet)) {
		$indexer = charger_fonction('indexer', 'inc/recommandations');
		$indexer($objet, $id_objet);
	}
}
